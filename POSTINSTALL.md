This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>
**Email**: admin@cloudron.local<br/>

Please change the admin password immediately by editing `/app/data/env.sh`.
Email address can be changed in `Settings` -> `General`.

