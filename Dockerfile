FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=2.1.0
RUN curl -L https://github.com/knadh/listmonk/releases/download/v2.1.0/listmonk_2.1.0_linux_amd64.tar.gz | tar zxvf -

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

