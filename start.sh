#!/bin/bash

set -eu

function update_setting_json() {
    # value is json value type. simple strings have to be quoted
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -t -c "INSERT INTO settings(key, value) VALUES('$1', '$2') ON CONFLICT (key) DO UPDATE SET value='$2'"
}

mkdir -p /app/data/uploads /run/listmonk /app/data/static/public /app/data/static/email-templates

touch /run/listmonk/config.toml # dummy file

if [[ ! -f /app/data/env.sh ]]; then
    echo "==> Installing on first run"
    cp /app/pkg/env.sh.template /app/data/env.sh
    source /app/data/env.sh
    /app/code/listmonk --yes --idempotent --install --config /run/listmonk/config.toml

    # one time settings
    update_setting_json app.notify_emails '["admin@cloudron.local"]'
    update_setting_json upload.filesystem.upload_path '"/app/data/uploads"'
    update_setting_json app.check_updates false
else
    source /app/data/env.sh
    /app/code/listmonk --yes --idempotent --upgrade --config /run/listmonk/config.toml
fi

echo "==> Updating settings"
update_setting_json app.root_url \"${CLOUDRON_APP_ORIGIN}\"
update_setting_json app.logo_url \"${CLOUDRON_APP_ORIGIN}/public/static/logo.png\"

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    echo "==> Configuring to use Cloudron email"
    update_setting_json app.from_email "\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Listmonk} <${CLOUDRON_MAIL_FROM}>\""
    smtp=$(cat <<-EOF
    [{"host": "${CLOUDRON_MAIL_SMTP_SERVER}", "port": ${CLOUDRON_MAIL_SMTPS_PORT}, "enabled": true, "password": "${CLOUDRON_MAIL_SMTP_PASSWORD}", "tls_type": "TLS", "username": "${CLOUDRON_MAIL_SMTP_USERNAME}", "max_conns": 10, "idle_timeout": "15s", "wait_timeout": "5s", "auth_protocol": "plain", "email_headers": [], "hello_hostname": "", "max_msg_retries": 2, "tls_skip_verify": true}]
EOF
)
    update_setting_json smtp "${smtp}"
fi

chown -R cloudron:cloudron /app/data /run/listmonk

echo "==> Starting listmonk"
exec gosu cloudron:cloudron /app/code/listmonk --config /run/listmonk/config.toml --static-dir=/app/data/static

